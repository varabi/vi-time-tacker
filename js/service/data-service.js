let dataService = {

    db: null,

    initDb: async function () {

        const tasks = {
            name: "tasks",
            columns: {
                id: {primaryKey: true, autoIncrement: true},
                name: {dataType: "string"},
                project: {dataType: "string"},
                time: {dataType: "number"},
                start: {dataType: "date_time"},
                running: {dataType: "number"}
            }
        }

        const db = {
            name: "vitimetracker",
            tables: [tasks],
            version: 1
        }

        this.db = new JsStore.Connection();
        this.db.initDb(db);
    }
};
