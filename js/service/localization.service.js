let localizationService = {

    localize: function() {
        document.querySelectorAll('[data-locale]').forEach(elem => {
            elem.innerText = chrome.i18n.getMessage(elem.dataset.locale);
        })
    },

    getMessage: function(code) {
        return chrome.i18n.getMessage(code);
    }
};