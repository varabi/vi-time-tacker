let taskService = {

    insert: async function(id, project, name) {

        const task = {
            id: id,
            project: project,
            name: name,
            time: 0
        };

        await dataService.db.insert({
            into: 'tasks',
            values: [task]
        });
    },

    remove: async function(id) {
        return await dataService.db.remove({
            from: 'tasks',
            where: {
                id: parseInt(id)
            }
        });
    },

    removeAll: async function() {
        return await dataService.db.remove({
            from: 'tasks'
        });
    },

    resetAll: async function() {
        await dataService.db.update({
            in: 'tasks',
            set: {
                time: 0
            }
        });
    },

    getAll: async function() {
        return await dataService.db.select({
            from: 'tasks',
            order: {
                by: 'id',
                type: 'desc'
            }
        });
    },

    get: async function(id) {
        return await dataService.db.select({
            from: 'tasks',
            where: {
                id: parseInt(id)
            }
        }).then((results) => {
            return results[0];
        });
    },

    update: async function(id, project, name, time) {
        await dataService.db.update({
            in: 'tasks',
            set: {
                project: project,
                name: name,
                time: time
            },
            where: {
                id: parseInt(id)
            }
        });
    },

    resetTime: async function(id) {
        await dataService.db.update({
            in: 'tasks',
            set: {
                time: 0
            },
            where: {
                id: parseInt(id)
            }
        });
    },

    startTask: async function(id, startTime) {
        await dataService.db.update({
            in: 'tasks',
            set: {
                running: 1,
                start: startTime
            },
            where: {
                id: id
            }
        });
    },

    stopTask: async function(id, time) {
        await dataService.db.update({
            in: 'tasks',
            set: {
                running: 0,
                time: time
            },
            where: {
                id: id
            }
        });
    },

    getRunning: async function() {
        return await dataService.db.select({
            from: 'tasks',
            where: {
                running: 1
            }
        });
    },

    getTotalTime: async function() {
        return await dataService.db.select({
            from: 'tasks',
            aggregate: {
                sum: 'time'
            }
        }).then(result => {
            return result.length !== 0 ? result[0]['sum(time)'] : 0;
        })
    }
};
