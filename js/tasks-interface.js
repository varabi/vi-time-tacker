let taskInterface = {

    intervals: [],

    options: {
        workTime: 0, //seconds
        exportFormat: 'hms',
        stopAfterTimeout: false
    },

    notification: {
        type: "basic",
        iconUrl: "../img/icons/vitt48.png",
        title: localizationService.getMessage("notification_title"),
        message: localizationService.getMessage("notification_message")
    },

    bind: function () {

        /* common elements
         ------------------------------------------------------------------------ */

        // cancel buttons click
        $(".cancel").live("click", function (e) {
            e.preventDefault();
            $("#" + $(this).attr("rel")).hide().hide().find("input:text").val("");
            $("#form-list").show();
        });

        /* create task
         ------------------------------------------------------------------------ */

        // create new task
        $(".create").live("click", function (e) {
            e.preventDefault();
            $(".form").hide();
            $("#new-task-error").hide();
            $("#form-create").slideDown().find("input[name='task-project-name']").focus();
        });

        // create new task > confirm click
        $("#button-create").live("click", function () {
            taskInterface.addTask();
        });

        // create new task > enter press
        $('#task-name').keydown(function (e) {
            if (e.keyCode === 13) {
                taskInterface.addTask();
            }
        });

        /* delete one task
         ------------------------------------------------------------------------ */

        // delete task
        $(".remove").live("click", function (e) {
            e.preventDefault();
            $(".form").hide();
            $("#button-remove").attr("rel", $(this).attr("rel"));
            $("#remove-confirm").html(localizationService.getMessage("delete_warning")
                + ($(this).data("name") ? $(this).data("name") : localizationService.getMessage("selected_item")) + "?");
            $("#form-remove").show();
        });

        // delete task > confirm deletion
        $("#button-remove").live("click", function () {
            $("#form-remove").hide();
            tasks.remove($(this).attr("rel"));
        });

        /* delete all tasks
         ------------------------------------------------------------------------ */

        // remove all tasks
        $(".remove-all").live("click", function (e) {
            e.preventDefault();
            $(".form").hide();
            $("#form-remove-all").slideDown();
        });

        // remove all tasks > confirm deletion
        $("#button-remove-all").live("click", function () {
            $("#form-remove-all").hide();
            tasks.removeAll();
        });

        /* reset all tasks
		 ------------------------------------------------------------------------ */

        // reset all tasks
        $(".reset-all").live("click", function (e) {
            e.preventDefault();
            $(".form").hide();
            $("#form-reset-all").slideDown();
        });

        // reset all tasks > confirm deletion
        $("#button-reset-all").live("click", function () {
            $("#form-reset-all").hide();
            tasks.resetAll();
        });

        /* export all tasks
         ------------------------------------------------------------------------ */

        $(".export-all").live("click", function (e) {
            taskService.getAll().then((results) => {
                taskInterface.export(results);
            });
        });

        /* update task name
         ------------------------------------------------------------------------ */

        // update task
        $(".update").live("click", function (e) {
            e.preventDefault();
            $(".form").hide();

            let id = $(this).attr("rel");

            taskService.get(id).then((task) => {
                $("#form-update :input[name='task-id']").val(id);
                $("#form-update :input[name='task-project-name']").val(task.project);
                $("#form-update :input[name='task-name']").val(task.name);
                $("#form-update :input[name='task-time']").val(taskInterface.hms(task.time));
                task.running === 1 ? $("#task-time-data").hide() : $("#task-time-data").show();
                $("#form-update").slideDown();
            });
        });

        // update task > save
        $("#button-update").live("click", function () {
            $("#form-update").hide();

            let id = $("#form-update :input[name='task-id']").val();
            let project = $("#form-update :input[name='task-project-name']").val();
            let name = $("#form-update :input[name='task-name']").val();
            let time = $("#form-update :input[name='task-time']").val();

            taskService.update(id, project, name, taskInterface.sec(time)).then(() => {
                taskInterface.index();
                taskInterface.count();
            });
        });

        /* reset task
         ------------------------------------------------------------------------ */

        $(".reset").live("click", function (e) {
            e.preventDefault();
            $(".form").hide();

            let id = $(this).attr("rel");

            taskService.resetTime(id).then(() => {
                taskInterface.index();
                taskInterface.count();
            });
        });


        $(".play").live("click", function(e) {
            e.preventDefault();
            taskInterface.toggleTimer(parseInt($(this).attr("rel")));
        })

    },

    index: function () {
        let out = "";

        taskService.getAll().then((results) => {
            if (results.length > 0) {
                results.forEach(task => {
                    out += '<p class="item' + (task.running === 1 ? ' running' : '') + '" id="item' + task.id + '" rel="' + task.id + '">';
                    out += '<label>' + task.name + '<br/><small>' + task.project + '</small></label>';
                    out += '<span id="task-edit-option-'+task.id+'"><a href="#" class="update" rel="' + task.id + '" title="'+localizationService.getMessage("btn_edit")+': '
                        + task.name + '" data-name="' + task.name + '">'+localizationService.getMessage("btn_edit")+'</a> | </span>';
                    out += '<span id="task-reset-option-' + task.id + '"';
                    if (task.running === 1) {
                        out += ' style="display: none;"'
                    }
                    out += '><a href="#" class="reset" rel="' + task.id + '" title="' + localizationService.getMessage("btn_reset") + ': '
                        + task.name + '" data-name="' + task.name + '">' + localizationService.getMessage("btn_reset") + '</a> | </span>';
                    out += '<span id="task-remove-option-'+task.id+'"><a href="#" class="remove" rel="' + task.id + '" title="'+localizationService.getMessage("btn_delete")+': '
                        + task.name + '" data-name="' + task.name + '">'+localizationService.getMessage("btn_delete")+'</a></span>';

                    if (task.running === 1) {
                        let start = new Date(task.start);
                        let dif = Number(task.time) + Math.floor((new Date().getTime() - start.getTime()) / 1000)
                        out += '<span class="timer">' + taskInterface.hms(dif) + '</span>';
                    } else {
                        out += '<span class="timer">' + taskInterface.hms(task.time) + '</span>';
                    }
                    out += '<a href="#" class="power play ' + (task.running === 1 ? 'running' : '') + '" title="Timer on/off" rel="' + task.id + '"></a>';
                    out += '</p>';

                    if (task.running === 1) {
                        taskInterface.startTask(task);
                    }
                });
            } else {
                out = "<p class=\"notask\"><label>"+localizationService.getMessage("no_tasks")+"</label></p>"
            }
            $("#form-list").empty().append(out).show();
        });
    },

    initOptions: function() {
        chrome.storage.sync.get(['workTime', 'exportFormat', 'stopAfterTimeout'], function(options) {
            taskInterface.options = {
                workTime: options.workTime !== undefined ? (options.workTime * 60 * 60) : 0,
                exportFormat: options.exportFormat !== undefined ? options.exportFormat : 'hms',
                stopAfterTimeout: options.stopAfterTimeout !== undefined && options.stopAfterTimeout
            };
        });
    },

    count: function () {
        taskService.getTotalTime().then((totalTime) => {
            $("#total-time-counter").text(taskInterface.hms(totalTime !== null ? totalTime : 0));
        });
    },

    init: function () {
        this.initOptions();
        localizationService.localize();
        this.bind();
        this.index();
        this.count();
        this.setBadgeColour();
        this.toggleRunText();
    },

    setBadgeColour: function () {
        chrome.action.setBadgeBackgroundColor({
            color: '#4285f4'
        });
    },

    toggleTimer: function (id, other) {
        taskService.get(id).then((task) => {
            $('#item' + id).toggleClass('running');
            $('#item' + id + ' .power').toggleClass('running');

            if (task.running === 1) {
                taskInterface.stopTask(task, function() {
                    taskInterface.count();
                    taskInterface.toggleRunText();
                });
            } else {
                taskInterface.startTask(task, function() {
                    taskInterface.count();
                    taskInterface.toggleRunText();
                    if (!other) {
                        taskInterface.stopOtherTasks(id);
                    }
                });
            }
        });
    },

    startTask: function (task, callback) {
        window.clearInterval(taskInterface.intervals[task.id]); // remove timer
        $("#task-reset-option-"+task.id).hide();

        let start = new Date(); // set start to NOW

        if (task.running === 1) {
            start = new Date(task.start);
        } else {
            taskService.startTask(task.id, start).then(() => {
                callback();
            })
        }

        let totalCounter = $("#total-time-counter");
        let totalCounterInitValue;
        let dbCheckCounter = 4;
        let notificationNotSent = true;

        // setup interval for counter
        taskInterface.intervals[task.id] = window.setInterval(function () {
            let timeFromStart = Math.floor((new Date().getTime() - start.getTime()) / 1000);
            let dif = Number(task.time) + timeFromStart;
            $('#item' + task.id + ' .timer').text(taskInterface.hms(dif));

            if (dbCheckCounter > 0) {
                taskService.getTotalTime().then((totalTime) => {
                    totalCounterInitValue = totalTime;
                    totalCounter.text(taskInterface.hms(totalCounterInitValue + timeFromStart));
                    dbCheckCounter--;
                });
            }
            else {
                totalCounter.text(taskInterface.hms(totalCounterInitValue + timeFromStart));
            }
            if (totalCounterInitValue !== undefined && taskInterface.showNotification(notificationNotSent, totalCounterInitValue, timeFromStart)) {
                chrome.notifications.create('workTimeLimit', taskInterface.notification);
                notificationNotSent = false;
                if (taskInterface.options.stopAfterTimeout) {
                    taskInterface.toggleTimer(task.id);
                }
            }
        }, 500);
    },

    showNotification: function(notificationNotSent, totalCounterInitValue, timeFromStart) {
        return notificationNotSent && taskInterface.options.workTime > 0
            && totalCounterInitValue + timeFromStart >= taskInterface.options.workTime;
    },

    stopOtherTasks: function(startedTaskId) {
        $('#form-list').find('.item .running').each(function() {
            let taskId = parseInt($(this).attr('rel'));
            if (startedTaskId !== taskId) {
                taskInterface.toggleTimer(taskId, true);
            }
        });
    },

    addTask: function() {
        let project = $("#form-create :input[name='task-project-name']").val();
        let taskName = $("#form-create :input[name='task-name']").val();
        if (project || taskName) {
            tasks.insert(taskInterface.nextID(), project, taskName);
            $("#form-create").hide().find("input:text").val("");
        }
        else {
            $("#new-task-error").show();
        }
    },

    stopTask: function (task, callback) {
        window.clearInterval(taskInterface.intervals[task.id]); // remove timer
        $("#task-reset-option-"+task.id).show();

        let start, stop, dif = 0;
        taskService.get(task.id).then((task) => {

            start = new Date(task.start); // read from DB
            stop = new Date(); // now
            dif = Number(task.time) + Math.floor((stop.getTime() - start.getTime()) / 1000); // time diff in seconds

            $('#item' + task.id + ' .timer').text(taskInterface.hms(dif));

            // update record
            taskService.stopTask(task.id, Number(dif)).then(() => {
                callback();
            });
        });
    },

    export: function(results) {
        let content = taskInterface.prepareExportContent(results);
        let blob = new Blob(content, {type: "text/plain"});
        let url = URL.createObjectURL(blob);

        chrome.downloads.download({
            url: url,
            filename: taskInterface.prepareFileName()
        });
    },

    prepareExportFileHeader: function (content) {
        content.push(
            taskInterface.appendStringToLength(localizationService.getMessage("export_file_title_date"),
                " ", 18));
        content.push(
            taskInterface.appendStringToLength(localizationService.getMessage("export_file_title_time"),
                " ", 8));
        content.push(localizationService.getMessage("export_file_title_task"));
        content.push("\n----------------------------------------------\n");
    },

    prepareExportContent: function(results) {
        let content = [];
        this.prepareExportFileHeader(content);
        const headerElements = content.length;

        if (results.length > 0) {
            results.forEach(task => {
                if (task.start !== undefined) {
                    let row = new Date(task.start).toISOString().slice(0, 10);
                    if (task.running === 1) {
                        let start = new Date(task.start);
                        let dif = Number(task.time) + Math.floor((new Date().getTime() - start.getTime()) / 1000);
                        row = taskInterface.appendStringToLength(row, taskInterface.formatExportTime(dif), 22);
                    } else {
                        row = taskInterface.appendStringToLength(row, taskInterface.formatExportTime(task.time), 22);
                    }
                    row += "    ";
                    if (task.project) {
                        if (task.name.trim() !== "") {
                            row += "[";
                        }
                        row += task.project;
                        if (task.name.trim() !== "") {
                            row += "] ";
                        }
                    }
                    row += task.name + "\n";
                    content.push(row);
                }
            });
            if (content.length === headerElements) {
                content.push(localizationService.getMessage("no_tasks"));
            }
        } else {
            content.push(localizationService.getMessage("no_tasks"));
        }
        return content;
    },

    formatExportTime: function(timeInSeconds) {
        if (taskInterface.options.exportFormat === 'm') {
            return taskInterface.minutes(timeInSeconds);
        }
        else if (taskInterface.options.exportFormat === 'h') {
            return taskInterface.hours(timeInSeconds);
        }
        return taskInterface.hms(timeInSeconds);
    },

    appendStringToLength(source, string, length) {
        let newSource = source;
        while ((newSource + string).length < length) {
            newSource += " ";
        }
        return newSource + string;
    },

    prepareFileName: function() {
        let now = Date.now();
        let offset = new Date(now).getTimezoneOffset() * 60000;
        let date = new Date(now-offset);
        return "ViTimeTracker-" + date.toISOString()
            .slice(0, 19)
            .replace("T", "_")
            .replace(new RegExp(":", 'g'), "-") + ".txt";
    },

    toggleRunText: function () {
        taskService.getRunning().then((results) => {
            if (results.length > 0) {
                chrome.action.setBadgeText({
                    text: 'RUN'
                });
            } else {
                chrome.action.setBadgeText({
                    text: ''
                });
            }
        });
    },

    // convert sec to hms
    hms: function (secs) {
        //secs = secs % 86400; // fix 24:00:00 overlay
        let time = [0, 0, secs], i;
        for (i = 2; i > 0; i--) {
            time[i - 1] = Math.floor(time[i] / 60);
            time[i] = time[i] % 60;
            if (time[i] < 10) {
                time[i] = '0' + time[i];
            }
        }
        if (time[0] < 10) {
            time[0] = "0" + time[0];
        }
        return time.join(':');
    },

    // convert h:m:s to sec
    sec: function (hms) {
        let t = String(hms).split(":");
        return Number(parseFloat(t[0] * 3600) + parseFloat(t[1]) * 60 + parseFloat(t[2]));
    },

    minutes: function(seconds) {
        return taskInterface.formatConvertedTime(seconds / 60);
    },

    hours: function(seconds) {
        return taskInterface.formatConvertedTime(seconds / 60 / 60);
    },

    formatConvertedTime: function(time) {
        return Number(Math.round((time + Number.EPSILON) * 100) / 100).toFixed(2);
    },

    nextID: function () {
        let id = localStorage['lastid']; // get last id from local storage
        if (id === undefined) {
            id = 1; // generate first ID
        } else {
            id++; // generate next ID
        }
        localStorage['lastid'] = id; // save to localStorage
        return id;
    }
};
