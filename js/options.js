$(function () {

    chrome.storage.sync.get(['workTime', 'exportFormat', 'stopAfterTimeout'], function(options) {
        let workTime = options.workTime !== undefined ? options.workTime : 0;
        $("#work-time").val(workTime);

        let exportFormat = options.exportFormat !== undefined ? options.exportFormat : 'hms';
        $("input:radio[name='format']").filter('[value='+exportFormat+']').prop('checked', true);

        let stopAfterTimeout = options.stopAfterTimeout !== undefined && options.stopAfterTimeout;
        $("input:checkbox[name='stop-after-timeout']").prop('checked', stopAfterTimeout);
    });

    $("#work-time").live("change", function() {
        let workingTimeError = $("#work-time-error");
        $(this).val() < 0 ? workingTimeError.show() : workingTimeError.hide();
    });

    $("#save").live("click", function() {
        let workTime = $("#work-time").val();
        if (workTime && workTime >= 0) {
            let options = {
                'workTime': workTime,
                'exportFormat': $("input:radio[name='format']:checked").val(),
                'stopAfterTimeout': $("input:checkbox[name='stop-after-timeout']").is(':checked')
            };
            chrome.storage.sync.set(options, showSavedAlert());
        }
    });

    $("#reset").live("click", function() {
        let options = {
            'workTime': 0,
            'exportFormat': 'hms',
            'stopAfterTimeout': false
        };
        chrome.storage.sync.set(options, function() {
            $("#work-time").val(options.workTime);
            $("input:radio[name='format']").filter('[value='+options.exportFormat+']').prop('checked', true);
            $("input:checkbox[name='stop-after-timeout']").prop('checked', options.stopAfterTimeout);
            $("#work-time-error").hide();
            showSavedAlert();
        });
    });

    function showSavedAlert() {
        let savedAlert = $("#saved-alert");
        savedAlert.show();
        setTimeout(function() {
            savedAlert.hide();
        }, 1500);
    }

    localizationService.localize();
});