let tasks = {

	insert: function (id, project, name) {
		taskService.insert(id, project, name).then(() => {
			taskInterface.index();
			taskInterface.count();
		});
	},

	remove: function (id) {
		taskService.remove(id).then(() => {
			window.clearInterval(taskInterface.intervals[id]);
			taskInterface.index();
			taskInterface.count();
		});
	},

	removeAll: function () {
		taskService.removeAll().then(() => {
			taskInterface.index();
			taskInterface.count();
		});
	},

    resetAll: function () {
		taskService.resetAll().then(() => {
			taskInterface.index();
			taskInterface.count();
		});
	}
};
