# Vi Time Tracker

Vi Time Tracker is Google Chrome Extension for time tracking.  
It is based on [Simple Time Tracker](https://github.com/afbora/SimpleTimeTracker)
by PixelTürk Web Studio.

[Install from Chrome Web Store](https://chrome.google.com/webstore/detail/vi-time-tracker/nceabdmobedkhoimhmpockclcfcbpfeo)

## Changelog

### 1.2.0 - 2024-01-07

* (update) Replacing WebSQL with IndexedDB
* (update) Compatibility with Manifest V3

### 1.1.0 - 2020-12-22

* (new) Export to txt file (+ time format configuration)
* (new) Option to stop the task when the working time has expired
* (update) More compact UI

### 1.0.1 - 2020-08-29

* (new) Multi-language support (+ polish translation)
* (fix) Time editing option only for stopped tasks
* (fix) Hidden time reset option for active task
* (fix) Creating a task requires its name or project name
* (fix) Preventing the browser from closing when saving the configuration
* (fix) The configuration reset did not clear the validation error

### 1.0.0 - 2020-08-23 (compared to the Simple Time Tracker)  

* (new) Options page - possibility to define working time
* (new) Notification of the end of working time
* (new) Only one task can be active at a time
* (new) Live update of total time
* (update) Changed application structure
